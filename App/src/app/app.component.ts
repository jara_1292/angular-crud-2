import { Component } from '@angular/core';
import { Article } from './models/article';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Crud de articulos en Angular';
  arrayArticles = [
    { code: 1, description: 'Art1', price: 20.50},
    { code: 2, description: 'Art2', price: 10.50},
    { code: 3, description: 'Art3', price: 30.50}
  ];

  selectedArticle: Article = new Article();


  selectArticle(article: Article) {
    this.selectedArticle = article;
  }

  addOrEdit() {
    if (isNullOrUndefined(this.selectedArticle.code)) {
      this.selectedArticle.code = this.arrayArticles.length + 1;
      this.arrayArticles.push(this.selectedArticle);
    }
    this.selectedArticle = new Article();
  }

  delete() {
    if (confirm('Delete?')) {
      this.arrayArticles = this.arrayArticles.filter(x => x !== this.selectedArticle);
      this.selectedArticle = new Article();
    }
  }
}
