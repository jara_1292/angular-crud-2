export class Article {
    code: number;
    description: string;
    price: number;
}
